package db

import (
	"errors"
	"os"

	"github.com/gofiber/fiber/v2"
)

type Contents []Content
type Content struct {
	File        string `mod:"trim" validate:"required"`
	Date        int64
	Type        string
	Size        string
	Title       string `mod:"trim"`
	Description string `mod:"trim"`
}

func (cs *Contents) LoadAll(file string) error {
	rows, err := db.Query("SELECT * FROM content WHERE File LIKE ?;", file)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		c := new(Content)
		if err := rows.Scan(&c.File, &c.Date, &c.Type, &c.Size, &c.Title, &c.Description); err != nil {
			return err
		}
		*cs = append(*cs, *c)
	}
	return nil
}

func (c *Content) Save(f *fiber.Ctx) error {
	form, err := f.MultipartForm()
	if err != nil {
		return fiber.NewError(fiber.StatusServiceUnavailable, err.Error())
	}
	var errArr []error
	for _, file := range form.File["files"] {
		file.Filename = validChars.ReplaceAllString(file.Filename, "-")
		if file.Filename == "" {
			errArr = append(errArr, errors.New("malformed filename"))
			continue
		}
		path := "./static/content/" + file.Filename
		if err := f.SaveFile(file, path); err != nil {
			errArr = append(errArr, errors.New(err.Error()))
			continue
		}
		if _, err := db.Exec(`INSERT INTO content (File, Type, Size) VALUES (?, ?, ?) ON CONFLICT(File) DO UPDATE SET Type = excluded.Type, Size = excluded.Size;`,
			file.Filename, file.Header.Get("Content-Type"), file.Size); err != nil {
			os.Remove(path)
			errArr = append(errArr, errors.New(err.Error()))
		}
	}
	if errArr != nil {
		return fiber.NewError(fiber.StatusServiceUnavailable, errors.Join(errArr...).Error())
	}
	return err
}

func (c *Content) Update(source string) error {
	c.File = validChars.ReplaceAllString(c.File, "-")
	if c.File == "" || source == "" {
		return fiber.NewError(fiber.StatusBadRequest, "file and source are required.")
	}
	if c.File != source {
		if _, err := os.Stat("./static/content/" + c.File); err == nil {
			return fiber.NewError(fiber.StatusBadRequest, "File Exists "+source)
		}
		if err := os.Rename("./static/content/"+source, "./static/content/"+c.File); err != nil {
			return fiber.NewError(fiber.StatusServiceUnavailable, err.Error())
		}
	}
	if _, err := db.Exec(`UPDATE content SET File = ?, Title = ?, Description = ? WHERE File = ?`, c.File, c.Title, c.Description, source); err != nil {
		return fiber.NewError(fiber.StatusServiceUnavailable, err.Error())
	}
	return nil
}

func (c *Content) Delete(file string) error {
	if err := os.Remove("./static/content/" + file); err != nil {
		return fiber.NewError(fiber.StatusServiceUnavailable, err.Error())
	}
	if _, err := db.Exec(`DELETE FROM content WHERE File = ?;`, file); err != nil {
		return fiber.NewError(fiber.StatusServiceUnavailable, err.Error())
	}
	return nil
}
