package db

import (
	"database/sql"

	"github.com/gofiber/fiber/v2"
)

type Markdowns []Markdown
type Markdown struct {
	Title    string `mod:"trim" validate:"required"`
	Date     int64
	Markdown string `mod:"trim" validate:"required"`
	Html     string `mod:"trim" validate:"required"`
}

func (ms *Markdowns) LoadAll(title string) error {
	rows, err := db.Query("SELECT * FROM markdown WHERE Title LIKE ?;", title)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		m := new(Markdown)
		if err := rows.Scan(&m.Title, &m.Date, &m.Markdown, &m.Html); err != nil {
			return err
		}
		*ms = append(*ms, *m)
	}
	return nil
}

func (m *Markdown) Load(title string) error {
	row := db.QueryRow(`SELECT * FROM markdown WHERE Title = ?;`, title)
	if err := row.Scan(&m.Title, &m.Date, &m.Markdown, &m.Html); err != nil && err != sql.ErrNoRows {
		return fiber.NewError(fiber.StatusServiceUnavailable, err.Error())
	}
	return nil
}

func (m *Markdown) Save() error {
	_, err := db.Exec(`INSERT OR REPLACE INTO markdown (Title, Markdown, Html) VALUES (?, ?, ?);`, m.Title, m.Markdown, m.Html)
	return err
}

func (m *Markdown) Delete(title string) error {
	_, err := db.Exec(`DELETE FROM markdown WHERE Title = ?;`, title)
	return err
}
