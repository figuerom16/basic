package db

import (
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/figuerom16/gosqliteadmin"
)

var (
	db         *gosqliteadmin.DB
	kvSetting  *gosqliteadmin.KV
	validChars = regexp.MustCompile("[^0-9a-zA-Z-()._]")
	S          *Settings
)

type Settings struct {
	SiteTitle     string
	SiteDesc      string
	SiteLogo      string
	AdminKey      string
	ProtectedKey  string
	BodyLimit     int
	MaxAge        int
	FiberPrefork  bool
	SchemaVersion int
	NavMenu       []string
}

func Close(err error) {
	if err != nil {
		db.Close()
		log.Fatalln(err)
	}
	if err := db.Close(); err != nil {
		log.Println(err)
	}
	log.Println("Database closed")
}

func Connect() {
	var err error
	db, err = gosqliteadmin.Connect(nil)
	if err != nil {
		Close(err)
	}
	log.Println("Database connected")
	if err := db.Migrate("migrations.sql"); err != nil {
		Close(err)
	}
	kvSetting = gosqliteadmin.KVTable(db, "setting", "Key", "Value")
	if len(os.Args) > 3 && os.Args[1] == "gosqliteadmin" {
		if err := kvSetting.KVVSet(map[string]string{os.Args[2]: os.Args[3]}); err != nil {
			Close(err)
		}
	}
	if err := UpdateSettings(nil); err != nil {
		Close(err)
	}
}

func UpdateSettings(settings map[string]string) error {
	s, err := kvSetting.KVVGetAll()
	if err != nil {
		return err
	}
	userVersion, err := db.GetUserVersion()
	if err != nil {
		return err
	}
	bodyLimit, err := strconv.Atoi(s["BodyLimit"])
	if err != nil {
		return err
	}
	maxAge, err := strconv.Atoi(s["MaxAge"])
	if err != nil {
		return err
	}
	fiberPrefork, err := strconv.ParseBool(s["FiberPrefork"])
	if err != nil {
		return err
	}
	S = &Settings{
		SiteTitle:     s["SiteTitle"],
		SiteDesc:      s["SiteDesc"],
		SiteLogo:      s["SiteLogo"],
		AdminKey:      s["AdminKey"],
		ProtectedKey:  s["ProtectedKey"],
		BodyLimit:     bodyLimit,
		MaxAge:        maxAge,
		FiberPrefork:  fiberPrefork,
		SchemaVersion: userVersion,
		NavMenu:       strings.Split(s["NavMenu"], "|"),
	}
	return nil
}

func Run(sql string) [][][]string { return db.RunSQLite(sql) }
