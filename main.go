package main

import (
	"basic/db"
	"basic/handle"
	"errors"
	"html/template"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/template/html/v2"
)

func main() {
	// Connect to SQLite and Load db.Site Settings
	db.Connect()

	// View Setup
	views := html.New("./view", ".html")
	views.AddFunc("unescape", func(s string) template.HTML { return template.HTML(s) })
	views.AddFunc("HasPrefix", func(s string, p string) bool { return strings.HasPrefix(s, p) })

	// App Setup
	app := fiber.New(fiber.Config{
		BodyLimit:         db.S.BodyLimit,
		IdleTimeout:       5 * time.Second,
		PassLocalsToViews: true,
		Prefork:           db.S.FiberPrefork,
		Views:             views,
		ViewsLayout:       "layout",
		ErrorHandler: func(c *fiber.Ctx, err error) error {
			code := fiber.StatusInternalServerError
			var e *fiber.Error
			if errors.As(err, &e) {
				code = e.Code
			}
			message := "ERROR " + strconv.Itoa(code) + ": " + err.Error()
			if code == fiber.StatusNotFound || code == fiber.StatusInternalServerError {
				return c.Status(code).Render("error", fiber.Map{"error": message})
			}
			return c.Status(code).SendString(message)
		},
	})

	app.Use(func(c *fiber.Ctx) error {
		c.Locals("s", db.S)
		c.Locals("path", c.Path())
		return c.Next()
	})
	app.Use(logger.New(), compress.New(), handle.Admin, recover.New())

	// Content Files
	app.Static("/", "./static", fiber.Static{
		Compress:  true,
		ByteRange: true,
		MaxAge:    db.S.MaxAge,
		Next:      handle.Content,
	})

	// Routes
	app.Get("/", handle.GetIndex)
	app.Get("/key", handle.SetCookie)
	app.Get("/p/:category", handle.GetCategoryMd)
	app.Get("/admin", handle.GetAdmin)
	app.Post("/admin", handle.PostAdmin)
	app.Put("/admin", handle.PutAdmin)
	app.Delete("/admin", handle.DeleteAdmin)
	app.Get("/admin/settings", handle.GetSettings)
	app.Post("/admin/settings", handle.PostSettings)
	app.Get("/admin/sql", handle.GetSQLite)
	app.Post("/admin/sql", handle.PostSQLite)
	app.Get("/admin/md/:title?", handle.GetAdminMd)
	app.Post("/admin/md/:title?", handle.PostAdminMd)

	// Server Run
	go func() {
		if err := app.Listen(":3000"); err != nil {
			log.Println(err)
		}
	}()

	// Graceful Shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	<-c
	start := time.Now()
	log.Println("Gracefully shutting down...")
	app.Shutdown()
	db.Close(nil)
	log.Printf("Gracefully closed %s", time.Since(start))
}
