htmx.on('htmx:beforeRequest', _=>{
	me('#error').innerText = me('#success').innerText = ''
})

htmx.on('htmx:afterRequest', e=>{
	if (e.detail.failed) me('#error').innerText = e.detail.xhr.responseText
	else setTimeout(() => {me('#success').innerText = ''}, 3000)
})

htmx.onLoad(_=>{
	// clear messages on click. Also copy to clipboard on error.
	me('#error').on('click', e=>{navigator.clipboard.writeText(me(e).innerText); me(e).innerText = '';})
	me('#success').on('click', e=>{me(e).innerText = ''})
})

window.onload=()=>{
	// Use links on mousedown instead of click
	any('a').on('mousedown', e=>{
		if (e.button !== 0) return
		e.preventDefault()
		e.target.click()
	})
}

window.addEventListener('popstate', _=> {
	if (window.location.pathname.startsWith('/admin')) location.href = window.location.pathname
})

window.onscroll=()=>{
	if (document.documentElement.scrollTop > 100) scroller.style.display = "block";
	else scroller.style.display = "none";
}

codeInput.registerTemplate("syntax-highlighted", codeInput.templates.hljs(hljs, [new codeInput.plugins.Indent(true, 4), new codeInput.plugins.AutoCloseBrackets()]))

function exportTable(table, sep) {
	const rows = [...table.rows]
	const data = rows.filter(row => row.style.display !== 'none').map(row => [...row.cells].map(cell => cell.innerText))
	data[0] = data[0].map(cell => cell.slice(0, -2))
	const blob = new Blob([data.map(row => row.join(sep)).join('\n')], {type: 'text/csv'})
	const a = document.createElement('a')
	a.href = URL.createObjectURL(blob)
	a.download = 'export.csv'
	a.click()
	URL.revokeObjectURL(a.href)
}

function showType(show, head) {
	if (show) for (let i = 0; i < head.cells.length; i++) head.cells[i].innerHTML = head.cells[i].innerHTML.replace(/<span style="display: none;">\[(.*?)\]<\/span>/g, '[$1]')
	else for (let i = 0; i < head.cells.length; i++) head.cells[i].innerHTML = head.cells[i].innerHTML.replace(/\[(.*?)\]/g, '<span style="display: none;">[$1]</span>')
}

function searchTable(table, term) {
	const rows = [...table.rows].slice(1)
	rows.forEach((row)=>{ 
		const found = [...row.cells].some(cell=>{
			const scripts = cell.getElementsByTagName('script')
			if (scripts.length > 0) return false
			return cell.innerText.includes(term)
		})
		row.style.display = found ? '' : 'none'
	})
}

function sortTable(head) {
	const arrow = head.innerText.substr(-1)
	const heads = head.parentElement
	const column = [...heads.cells].indexOf(head)
	const body = head.parentElement.parentElement
	const rowsArray = [...body.rows].slice(1)
	for (let e of heads.cells) {if (['►','▲','▼'].includes(e.innerText.substr(-1))) e.innerText=e.innerText.slice(0, -1) + '►'}
	if (arrow === '▼') {
		head.innerText = head.innerText.slice(0, -1) + '▲'
		rowsArray.sort((a, b)=>b.cells[column].innerHTML.localeCompare(a.cells[column].innerHTML, undefined, { numeric: true }))
	}
	else {
		head.innerText = head.innerText.slice(0, -1) + '▼'
		rowsArray.sort((a, b)=>a.cells[column].innerHTML.localeCompare(b.cells[column].innerHTML, undefined, { numeric: true }))
	}
	body.innerHTML = ''
	body.appendChild(heads)
	for (let row of rowsArray) body.appendChild(row)
}

function generateKey() {
	const length = 64
	const chars = '-_.!~*().0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
	const charsLength = chars.length
	let key = ''
	for (let i = 0; i < length; i++) key += chars[Math.floor(Math.random() * charsLength)]
	return key
}

function mtotals() {
	me('#mtotals').innerText = 'TOTALS\nItems ' + (me('#tmarkdown').rows.length - 1)
}

function ctotals() {
	me('#ctotals').innerText = 'TOTALS\nItems ' + (me('#tcontent').rows.length - 1)+ '\nMB ' +
		Array.from(me('#tcontent').rows).slice(1).reduce((acc, row)=>acc + parseFloat(row.cells[5].innerText), 0).toFixed(3)
}