# !/bin/bash
# Update static files

# JS
wget https://unpkg.com/htmx.org@2.0.2 -O static/js/htmx.min.js
wget https://cdn.jsdelivr.net/gh/gnat/surreal@main/surreal.js -O static/js/surreal.js
wget https://cdn.jsdelivr.net/npm/marked/marked.min.js -O static/js/marked.min.js
wget https://raw.githubusercontent.com/SortableJS/Sortable/master/Sortable.min.js -O static/js/sortable.min.js
wget https://unpkg.com/@highlightjs/cdn-assets@latest/highlight.min.js -O static/js/highlight.min.js
wget https://unpkg.com/@highlightjs/cdn-assets@latest/languages/sql.min.js -O static/js/sql.min.js
wget https://cdn.jsdelivr.net/gh/WebCoder49/code-input@latest/code-input.min.js -O static/js/code-input.min.js
wget https://raw.githubusercontent.com/WebCoder49/code-input/main/plugins/auto-close-brackets.min.js -O static/js/auto-close-brackets.min.js
wget https://raw.githubusercontent.com/WebCoder49/code-input/main/plugins/indent.min.js -O static/js/indent.min.js

# CSS
wget https://cdn.jsdelivr.net/npm/@picocss/pico@2/css/pico.fluid.classless.min.css -O static/css/pico.fluid.classless.min.css
wget https://cdn.jsdelivr.net/npm/@picocss/pico@2/css/pico.colors.min.css -O static/css/pico.colors.min.css
wget https://cdn.jsdelivr.net/gh/WebCoder49/code-input@latest/code-input.min.css -O static/css/code-input.min.css
wget https://unpkg.com/@highlightjs/cdn-assets@latest/styles/stackoverflow-dark.min.css -O static/css/stackoverflow-dark.min.css
