--gosqliteadmin
CREATE TABLE IF NOT EXISTS 'setting' (
	Key TEXT PRIMARY KEY,
	Value TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS 'content' (
	File TEXT PRIMARY KEY,
	Date INTEGER NOT NULL DEFAULT (strftime('%s', 'now')),
	Type TEXT NOT NULL,
	Size TEXT NOT NULL,
	Title TEXT NOT NULL DEFAULT '',
	Description TEXT NOT NULL DEFAULT ''
);
CREATE TABLE IF NOT EXISTS 'markdown' (
	Title TEXT PRIMARY KEY,
	Date INTEGER NOT NULL DEFAULT (strftime('%s', 'now')),
	Markdown TEXT NOT NULL,
	Html TEXT NOT NULL
);

--gosqliteadmin
INSERT OR IGNORE INTO 'setting' ('key', 'value') VALUES
('SiteTitle', 'Basic'),
('SiteDesc', 'Basic'),
('SiteLogo', 'mylogo.svg'),
('AdminKey', 'admin'),
('ProtectedKey', ''),
('BodyLimit', '100000000'),
('MaxAge', '0'),
('FiberPrefork', 'false'),
('NavMenu', '');