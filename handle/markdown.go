package handle

import (
	"basic/db"
	"sort"

	"github.com/gofiber/fiber/v2"
)

func GetIndex(c *fiber.Ctx) error {
	markdown := new(db.Markdown)
	if err := markdown.Load("INDEX"); err != nil {
		return err
	}
	return c.Render("category", fiber.Map{"markdown": markdown})
}

func GetCategoryMd(c *fiber.Ctx) error {
	category := c.Params("category")
	markdown := new(db.Markdown)
	if err := markdown.Load(category); err != nil {
		return err
	}
	contents := db.Contents{}
	err := contents.LoadAll(category + "-%")
	if err != nil {
		return err
	}
	sort.Slice(contents, func(i, j int) bool {
		return contents[i].File < contents[j].File
	})
	return c.Render("category", fiber.Map{"markdown": markdown, "contents": contents})
}
