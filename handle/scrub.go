package handle

import (
	"errors"
	"strings"

	"github.com/go-playground/mold/v4/modifiers"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

var (
	validate = validator.New(validator.WithRequiredStructEnabled())
	conform  = modifiers.New()
)

// data Validations
func Scrub(c *fiber.Ctx, model any) error {
	if err := c.BodyParser(model); err != nil {
		return fiber.NewError(fiber.StatusServiceUnavailable, err.Error())
	}
	if err := conform.Struct(c.Context(), model); err != nil {
		return fiber.NewError(fiber.StatusServiceUnavailable, err.Error())
	}
	if err := validate.Struct(model); err != nil {
		var errArr []error
		for _, err := range err.(validator.ValidationErrors) {
			var msg []string
			switch err.Tag() {
			case "required":
				msg = []string{err.Field(), "ensure input is not empty."}
			// case "email":
			// 	msg = []string{err.Field(), "ensure email address is valid."}
			// case "alphanum":
			// 	msg = []string{err.Field(), "ensure input is alphanumeric."}
			// case "len":
			// 	msg = []string{err.Field(), "ensure input has exactly", err.Param(), "characters."}
			// case "min":
			// 	msg = []string{err.Field(), "ensure input has at least", err.Param(), "characters."}
			// case "max":
			// 	msg = []string{err.Field(), "ensure input has at most", err.Param(), "characters."}
			// case "date":
			// 	msg = []string{err.Field(), "Must be less than current date and in the correct format."}
			default:
				msg = []string{err.Field(), err.Type().String(), err.Tag(), err.Param()}
			}
			errArr = append(errArr, errors.New(strings.Join(msg, " ")))
		}
		return errors.Join(errArr...)
	}
	return nil
}
