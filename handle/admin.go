package handle

import (
	"basic/db"
	"net/url"
	"strings"

	"github.com/gofiber/fiber/v2"
)

func GetAdmin(c *fiber.Ctx) error {
	markdowns := new(db.Markdowns)
	if err := markdowns.LoadAll("%"); err != nil {
		return err
	}
	contents := new(db.Contents)
	if err := contents.LoadAll("%"); err != nil {
		return err
	}
	return c.Render("admin", fiber.Map{"markdowns": markdowns, "contents": contents})
}

func PostAdmin(c *fiber.Ctx) error {
	c.Append("HX-Trigger", "tcontent")
	content := new(db.Content)
	if err := content.Save(c); err != nil {
		return err
	}
	return c.SendString("Uploaded successfully.")
}

func PutAdmin(c *fiber.Ctx) error {
	content := new(db.Content)
	if err := Scrub(c, content); err != nil {
		return err
	}
	if err := content.Update(c.Query("source")); err != nil {
		return err
	}
	return c.SendString("Edited successfully.")
}

func DeleteAdmin(c *fiber.Ctx) error {
	file := c.Query("file")
	if file != "" {
		content := new(db.Content)
		if err := content.Delete(file); err != nil {
			return err
		}
	}
	title := c.Query("title")
	if title != "" {
		markdown := new(db.Markdown)
		if err := markdown.Delete(title); err != nil {
			return err
		}
	}
	return c.SendString("Deleted successfully.")
}

func GetAdminMd(c *fiber.Ctx) error {
	title := c.Params("title")
	markdown := new(db.Markdown)
	if title == "" {
		return c.Render("editor", fiber.Map{"markdown": markdown})
	}
	if err := markdown.Load(title); err != nil {
		return err
	}
	return c.Render("editor", fiber.Map{"markdown": markdown})
}

func PostAdminMd(c *fiber.Ctx) error {
	md := new(db.Markdown)
	if err := Scrub(c, md); err != nil {
		return err
	}
	if err := md.Save(); err != nil {
		return err
	}
	return c.SendString("Markdown saved successfully.")
}

func GetSettings(c *fiber.Ctx) error {
	return c.Render("settings", fiber.Map{})
}

func PostSettings(c *fiber.Ctx) error {
	values, err := url.ParseQuery(string(c.Body()))
	if err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}
	settings := make(map[string]string)
	for key, value := range values {
		if len(value) > 0 {
			settings[key] = strings.Join(value, "|")
		}
	}
	if err := db.UpdateSettings(settings); err != nil {
		return err
	}
	return c.SendString("Updated settings successfully.")
}

func GetSQLite(c *fiber.Ctx) error {
	return c.Render("sql", fiber.Map{})
}

func PostSQLite(c *fiber.Ctx) error {
	sql := strings.TrimSpace(c.FormValue("sql"))
	if sql == "" {
		return c.Status(fiber.StatusBadRequest).SendString("No SQL provided.")
	}
	return c.Render("result", fiber.Map{"result": db.Run(sql)}, "")
}
