package handle

import (
	"basic/db"
	"strings"

	"github.com/gofiber/fiber/v2"
)

func Admin(c *fiber.Ctx) error {
	s := db.S
	path := strings.Split(c.Path(), "/")[1]
	if path == "admin" {
		cookie := c.Cookies("A_" + s.SiteTitle)
		if s.AdminKey == "" || s.AdminKey != cookie {
			return fiber.NewError(fiber.StatusUnauthorized, "You are not authorized.")
		}
	}
	return c.Next()
}

func SetCookie(c *fiber.Ctx) error {
	s := db.S
	site, key := "", ""
	if s.AdminKey != "" {
		site = "A_" + s.SiteTitle
		key = s.AdminKey
	} else if s.ProtectedKey != "" {
		site = s.SiteTitle
		key = s.ProtectedKey
	} else {
		return fiber.NewError(fiber.StatusUnauthorized, "Bad Key.")
	}
	c.Cookie(&fiber.Cookie{
		Name:     site,
		Value:    key,
		MaxAge:   2147483647,
		HTTPOnly: true,
	})
	return c.Redirect("/admin")
}

func Content(c *fiber.Ctx) bool {
	s := db.S
	file := strings.Split(c.Path(), "/")
	if len(file) == 3 && file[1] == "content" && file[2][0] == '_' &&
		c.Cookies("A_"+s.SiteTitle) != s.AdminKey && c.Cookies(s.SiteTitle) != s.ProtectedKey {
		return true
	}
	return false
}
