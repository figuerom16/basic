#!/bin/bash
# Pull, build and restart the service if there are changes
# Also update Go if there is a new version

output=$(git pull)
if [[ "$output" == *"Already up to date." ]]; then
	exit 0
fi

go_current=$(go version | cut -d ' ' -f3)
go_new=$(grep ^go go.mod | tr -d ' ')
if [[ "$go_current" != "$go_new" ]]; then
	wget https://go.dev/dl/$go_new.linux-amd64.tar.gz
	rm -rf /usr/local/go && tar -C /usr/local -xzf $go_new.linux-amd64.tar.gz
	rm $go_new.linux-amd64.tar.gz
fi

go build && systemctl restart $(basename $(pwd))